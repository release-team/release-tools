#!/bin/bash

SUITE="$1"

SECURITYBASE="https://security.debian.org/dists/${SUITE}-security/"
TMPDIR="/srv/release.debian.org/tmp"

: > ${TMPDIR}/${SUITE}-Packages-security
: > ${TMPDIR}/${SUITE}-Packages-proposed
: > ${TMPDIR}/${SUITE}-Sources-security
: > ${TMPDIR}/${SUITE}-Sources-proposed

for component in $(dak admin s-c list-component ${SUITE}); do
	for arch in $(dak admin s-a list-arch ${SUITE}); do
		url=${SECURITYBASE}/${component}/binary-${arch}/Packages.xz
		file=${TMPDIR}/${arch}-Packages.xz
		wget $url -q -O ${file}
		[ -s "$file" ] || continue
		xzcat $file >> ${TMPDIR}/${SUITE}-Packages-security
		echo >> ${TMPDIR}/${SUITE}-Packages-security

		for file in \
		  /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/${component}/binary-${arch}/Packages.xz \
		  /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/${component}/binary-${arch}/Packages.xz \
		  /srv/ftp-master.debian.org/mirror/dists/${SUITE}/${component}/binary-${arch}/Packages.xz; do
			[ -s "$file" ] || continue
			xzcat $file >> ${TMPDIR}/${SUITE}-Packages-proposed
			echo >> ${TMPDIR}/${SUITE}-Packages-proposed
		done
	done

	wget -q ${SECURITYBASE}/${component}/source/Sources.xz -O - | xzcat >> ${TMPDIR}/${SUITE}-Sources-security

	for file in /srv/ftp-master.debian.org/mirror/dists/${SUITE}-proposed-updates/${component}/source/Sources.xz \
		    /srv/ftp-master.debian.org/policy/dists/${SUITE}-new/${component}/source/Sources.xz \
	            /srv/ftp-master.debian.org/mirror/dists/${SUITE}/${component}/source/Sources.xz; do
	            [ -s "$file" ] && xzcat $file >> ${TMPDIR}/${SUITE}-Sources-proposed
	done
done
