#!/usr/bin/env python3
import apt_pkg
import os
import sys
import time

apt_pkg.init_config()
apt_pkg.init_system()

suite = len(sys.argv) > 1 and sys.argv[1] or "stable"

basedir = '/srv/release.debian.org'
tmpdir = '%s/tmp' % (basedir)
template_filename = '%s/template' % (os.path.dirname(__file__))
output_filename = '%s/www/proposed-updates/missing-security-%s.html' % \
                  (basedir, suite)


def parse(file, arch=None, ret=None):
    if ret is None:
        ret = {}
    with open(file) as f:
        Parse = apt_pkg.TagFile(f)
    step = Parse.step
    section = Parse.section
    while step() == 1:
        p = section.get("Package")
        s = section.get("Source")
        if s:
            if s.find(' (') != -1:
                p = s[:s.find(' ')]
            else:
                p = s
        a = arch or section.get("Architecture")
        ret.setdefault(p, {})
        if a not in ret[p] or \
           apt_pkg.version_compare(section.get("Version"), ret[p][a]) > 0:
            ret[p][a] = section.get("Version")
    return ret


sec = parse('%s/%s-Packages-security' % (tmpdir, suite))
sec = parse('%s/%s-Sources-security' % (tmpdir, suite), 'source', sec)
prop = parse('%s/%s-Packages-proposed' % (tmpdir, suite))
prop = parse('%s/%s-Sources-proposed' % (tmpdir, suite), 'source', prop)


def check(sec, prop):
    result = {}
    for i in sec:
        for a in sec[i]:
            if i not in prop or \
               a not in prop[i] or \
               apt_pkg.version_compare(sec[i][a], prop[i][a]) > 0:
                prop.setdefault(i, {})
                prop[i].setdefault(a, "n/a")
                key = '%s/%s' % (i, '_source' if a == 'source' else a)
                result[key] = (i, a)
    return result


missing_packages = ["<li>{}/{} ({} vs {})</li>".format(package, arch,
                                                       sec[package][arch],
                                                       prop[package][arch]
                                                       )
                    for key, (package, arch)
                    in sorted(check(sec, prop).items())]
if missing_packages:
    missing_packages.insert(0, '<ul>')
    missing_packages.append('</ul>')

with open(template_filename, 'r') as template_file:
    template = template_file.read()

template = template.replace('$SUITE$', suite)
template = template.replace('$LIST$', '\n'.join(missing_packages))
template = template.replace('$TIME$',
                            time.strftime("%Y-%m-%dT%H:%M:%SZ",
                                          time.gmtime())
                            )

with open(output_filename, 'w') as output_file:
    output_file.write(template)
