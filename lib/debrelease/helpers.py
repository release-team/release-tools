## vim:set et ts=4 sw=4:
#
# helpers.py: convenience wrappers around generation of various diffs
#
# (C) Copyright 2019 Adam D. Barratt <adam@adam-barratt.org.uk>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301  USA.

import email.message
import gzip
import logging
import os.path
import re

from enum import Enum, unique

from debrelease import IGNORABLE_TIME_DELTA, FileNotFoundException, SPECIAL_ARCHES, send_mail
from debrelease.debbindiff import DebDiffFactory as DebBinDiffFactory
from debrelease.debcheck import DebCheckFactory
from debrelease.debdiff import DebDiffFactory
from debrelease.lintian import LintianFactory


@unique
class DiffType(Enum):
    debdiff = 1
    debbindiff = 2
    debcheck = 3
    lintian = 4


class Helpers():
    def __init__(self, projectB, config, suite):
        self.logger = logging.getLogger('debrelease.helpers')
        self.logger.addHandler(logging.NullHandler())
        self.logger.debug('Helpers: __init__')

        self.suite = suite
        self.projectb = projectB
        self.config = config

        self.compress_diffs = config.getboolean(suite,
                                                'compress_diffs',
                                                fallback='yes')

        self.differs = {}

        if config.has_option(suite, 'diff_output'):
            self.differs[DiffType.debdiff] = DebDiffFactory(projectB,
                                                            config,
                                                            suite)
        if config.has_option(suite, 'bin_diff_output'):
            self.differs[DiffType.debbindiff] = DebBinDiffFactory(projectB,
                                                                  config,
                                                                  suite)
        if config.has_option(suite, 'debcheck_output'):
            self.differs[DiffType.debcheck] = DebCheckFactory(projectB,
                                                              config,
                                                              suite)
        if config.has_option(suite, 'lintian_output'):
            self.differs[DiffType.lintian] = LintianFactory(projectB,
                                                            config,
                                                            suite)

    def make_and_check_diff(self,
                            diff_type,
                            package,
                            version,
                            architecture,
                            package_info):
        pass

    # return = (pending:bool, interesting:bool, error:exception)
    def do_binary_diff(self, package, version, architecture, package_info):
        versions = _version_split(version, package_info)
        debbindiffer = self.differs.get(DiffType.debbindiff)
        if not debbindiffer:
            return (False, False, None)

        try:
            self.logger.debug("Considering binary diff for %s_%s/%s",
                              package,
                              versions['display'],
                              architecture)
            if 'superseded' not in package_info:
                debbindiffer.create_diff_file(package,
                                              versions['display'],
                                              versions['source'],
                                              architecture,
                                              against_overlay=package_info['diff_against_overlay'],
                                              compressed=self.compress_diffs,
                                              )
                debbindiffer.create_diff_file(package,
                                              versions['display'],
                                              versions['source'],
                                              architecture,
                                              against_overlay=package_info['diff_against_overlay'],
                                              compressed=self.compress_diffs,
                                              html_output=True,
                                              apply_filter=True,
                                              )
            filespec = debbindiffer.filename(package,
                                             versions['display'],
                                             architecture,
                                             self.compress_diffs,
                                             True)
            self.logger.debug("Checking debdiff output")
            if os.path.isfile(filespec):
                if self.compress_diffs:
                    debbindiff_output_file = gzip.open(filespec)
                else:
                    debbindiff_output_file = open(filespec, 'rb')
                debbindiff_out = debbindiff_output_file.read().decode('utf-8')
                debbindiff_output_file.close()
                match = re.search("After applying filtering, no changes remain",
                                  debbindiff_out,
                                  re.MULTILINE)
                if not match:
                    self.logger.debug("Marking %s for attention", architecture)
                return (False, match is None, None)
            else:
                return ('superseded' not in package_info, False, None)
        except FileNotFoundException as exc:
            if exc.filename.startswith('/') or \
              package_info.get('upload_age', 0) > IGNORABLE_TIME_DELTA or \
              package_info.get('upload_age', 0) == 0:
                return (True, False, exc)
            else:
                return (True, False, None)
        except Exception as exc:
            return (True, False, exc)

    # return = (pending:bool, interesting:bool, error:exception)
    def do_debcheck(self, package, version, architecture, package_info):
        versions = _version_split(version, package_info)
        debchecker = self.differs.get(DiffType.debcheck)
        if not debchecker:
            return (False, False, None)

        # If the source package builds architecture-independent
        # packages that are not yet available, architecture-dependent
        # checks should be delayed, otherwise any arch-dep ->
        # -indep relationships will be broken
        delay_checks = 'all' in package_info.get('missing_builds', [])

        # If there are both architecture-dependent and -independent
        # packages, the -indep packages will get tested alongside one
        # of the other architectures, so should not be checked
        # separately.
        skip_debcheck = 'superseded' in package_info or \
                        (architecture == 'all' and
                         package_info['architectures'].difference(SPECIAL_ARCHES)
                         != set([]))
        skip_debcheck = skip_debcheck or delay_checks

        self.logger.debug("Considering debcheck for %s_%s/%s", package,
                          versions['display'], architecture)

        filespec = debchecker.filename(package,
                                       versions['display'],
                                       architecture)
        try:
            if not os.path.isfile(filespec) and not skip_debcheck:
                debcheck = debchecker.check_packages(
                    package,
                    versions['display'],
                    versions['source'],
                    architecture
                )
                debcheck.create_output_file()
            self.logger.debug("Checking debcheck output")
            if os.path.isfile(filespec):
                with open(filespec, encoding='utf-8') as debcheck_output_file:
                    debcheck_output = debcheck_output_file.read()
                match = re.search("status: broken",
                                  debcheck_output,
                                  re.MULTILINE)
                if match:
                    self.logger.debug("Marking %s for attention", architecture)
                return (False, match is not None, None)
            elif delay_checks and 'superseded' not in package_info:
                # We will be running the check, so should flag it
                # as expected
                return (True, False, None)
            else:
                return (not skip_debcheck, False, None)
        except FileNotFoundException as exc:
            if exc.filename.startswith('/') or \
              package_info.get('upload_age', 0) > IGNORABLE_TIME_DELTA or \
              package_info.get('upload_age', 0) == 0:
                return (True, False, exc)
            else:
                return (True, False, None)
        except Exception as exc:
            return (True, False, exc)

    # return = (pending:bool, interesting:bool, error:exception)
    def do_lintian_diff(self, package, version, architecture, package_info):
        versions = _version_split(version, package_info)
        lintian = self.differs.get(DiffType.lintian)
        if not lintian:
            return (False, False, None)

        self.logger.debug("Considering lintian diff for %s_%s/%s", package,
                          versions['display'], architecture)
        try:
            if 'superseded' not in package_info:
                lintian.create_diff_file(package,
                                         versions['display'],
                                         versions['source'],
                                         architecture,
                                         against_overlay=package_info['diff_against_overlay'],
                                         compressed=self.compress_diffs)
            filespec = lintian.diff_filename(package,
                                             versions['display'],
                                             architecture,
                                             compressed=self.compress_diffs)
            self.logger.debug("Checking lintian output")
            if os.path.isfile(filespec):
                if self.compress_diffs:
                    lintian_diff_output_file = gzip.open(filespec)
                else:
                    lintian_diff_output_file = open(filespec, 'rb')
                lintian_diff_out = lintian_diff_output_file.read().decode('utf-8')
                lintian_diff_output_file.close()
                match = re.search("^No differences found",
                                  lintian_diff_out,
                                  re.MULTILINE)
                if not match:
                    self.logger.debug("Marking %s for attention", architecture)
                return (False, match is None, None)
            else:
                return ('superseded' not in package_info, False, None)
        except FileNotFoundException as exc:
            if exc.filename.startswith('/') or \
              package_info.get('upload_age', 0) > IGNORABLE_TIME_DELTA or \
              package_info.get('upload_age', 0) == 0:
                return (True, False, exc)
            else:
                return (True, False, None)
        except Exception as exc:
            return (True, False, exc)

    # return = (pending:bool, interesting:bool, error:exception)
    def do_source_debdiff(self, package, version, package_info):
        debdiffer = self.differs.get(DiffType.debdiff)
        if not debdiffer:
            return (False, False, None)
        self.logger.debug("Considering debdiff for %s_%s", package, version)
        try:
            filename = debdiffer.create_diff_file(package,
                                                  version,
                                                  package_info['diff_against_overlay'],
                                                  self.compress_diffs,
                                                  )
            if filename:
                self.send_diff_mail(self.suite, package, version, filename)
            # source debdiffs are always interesting
            return (False, True, None)
        except FileNotFoundException as exc:
            if exc.filename.startswith('/') or \
              package_info.get('upload_age', 0) > IGNORABLE_TIME_DELTA or \
              package_info.get('upload_age', 0) == 0:
                return (True, False, exc)
            else:
                return (True, False, None)
        except Exception as exc:
            return (True, False, exc)

    def send_diff_mail(self, suite, package, version, diff_filename):
        if self.config.has_option(suite, 'diff_mail_to'):
            msg = email.message.Message()
            msg['subject'] = 'New %s diff: %s %s' % (suite, package, version)
            msg['to'] = self.config.get(suite, 'diff_mail_to')
            msg['from'] = self.config.get('mail', 'from')
            if self.config.getboolean(suite, "compress_diffs", fallback="yes"):
                diff = gzip.open(diff_filename, 'r')
            else:
                diff = open(diff_filename, 'rb')
            try:
                msg.set_payload(diff.read(), 'utf-8')
            finally:
                diff.close()
            send_mail(msg)


def _version_split(version, package_info):
    fullversion = package_info.get('fullversion', version)
    sourceversion = package_info.get('source_version', fullversion)
    if 'binNMU' in package_info:
        displayversion = version
    else:
        displayversion = fullversion
    return {
        'display': displayversion,
        'full': fullversion,
        'source': sourceversion,
        'version': version,
    }
