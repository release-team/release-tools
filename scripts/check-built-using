#!/usr/bin/python3

# To the extent possible under law, the author(s) have dedicated all copyright
# and related and neighboring rights to this software to the public domain
# worldwide. This software is distributed without any warranty.

from __future__ import print_function

import psycopg2
from apt_pkg import parse_depends

q = """WITH binaries_suite_arch AS (
        SELECT bin_associations.id, binaries.id AS bin, binaries.package, binaries.version,
          binaries.source, bin_associations.suite, suite.suite_name, binaries.architecture,
          architecture.arch_string
        FROM (((binaries JOIN bin_associations ON ((binaries.id = bin_associations.bin)))
          JOIN suite ON ((suite.id = bin_associations.suite)))
          JOIN architecture ON ((binaries.architecture = architecture.id)))
       )
       SELECT bsa.package, bsa.version, bsa.arch_string, bm.value
       FROM binaries_suite_arch as bsa, suite as s, metadata_keys as mk, binaries_metadata as bm
       WHERE s.suite_name = 'testing'
         AND s.id = bsa.suite
	 AND bsa.bin = bm.bin_id
	 AND bm.key_id = mk.key_id
	 AND mk.key = 'Built-Using'
"""

conn = psycopg2.connect("service=projectb")
cur = conn.cursor()
cur.execute(q)

results = cur.fetchall()

for package, version, arch, built_using in results:
	rel = parse_depends(built_using)
	for pkg in rel:
		# no OR dependency in Built-Using
		assert len(pkg) == 1, pkg
		pkg = pkg[0]
		pkgname, ver, op = pkg
		assert ver is not None, pkg
		assert op == '=', pkg
		q = """
		SELECT version from source_suite
		WHERE source = '%s'
		AND suite_name = 'testing'
		"""
		cur.execute(q % pkgname)
		versions = [row[0] for row in cur.fetchall()]
		if ver not in versions:
			print("%s_%s/%s Built-Using %s %s, but testing has %s" %
			      (package, version, arch, pkgname, ver, ' '.join(versions)))
