#!/usr/bin/python3
#-*- coding: utf8
#
# create autoremoval hints based from udd
# 
# usage:
# make_autoremoval_hints.py [timestamp]
#   if timestamp is specified, it will be used to check if the removal date is
#   past, instead of the current timestamp

import sys
from psycopg2 import connect
from psycopg2.extras import DictCursor
from datetime import datetime
from datetime import timedelta

if len(sys.argv) > 2:
    print("make_autoremoval_hints.py [timestamp]")
    sys.exit(1)

now = datetime.utcnow()
debug = 0

if len(sys.argv) > 1:
    removaldate = datetime.utcfromtimestamp(float(sys.argv[1]))
else:
    removaldate = now

query = "select * from testing_autoremovals order by source";

conn = connect(service='udd')
cur = conn.cursor(cursor_factory=DictCursor)
cur.execute(query)
rows = cur.fetchall()
cur.close()
conn.close()

# if the data is older than 1 day ago, we ignore it, because some update must
# be lagging
old = now - timedelta(1)

print("# generated on "+now.isoformat()+"\n")
for row in rows:
    source_removal = datetime.utcfromtimestamp(row['removal_time'])

    prefix = ""
    if (source_removal > removaldate):
        prefix = "#"
    if (datetime.utcfromtimestamp(row['last_checked']) < old):
        prefix = "#";
        if not debug:
            # sanity check: don't add hint if bug data is outdated
            print("#bug data for "+row['source']+" out-of-date: hint disabled\n")

    if prefix and not debug:
        continue

    if (source_removal > removaldate):
        print("# only remove after "+source_removal.isoformat())
    hint = "remove "+row['source']+'/'+row['version']+"\n"
    if row['buggy_deps'] != "":
        print("# "+row['bugs_deps']+" in "+row['buggy_deps'])
    if row['bugs'] != "":
        print("# "+row['bugs'])
    print(prefix+hint)

