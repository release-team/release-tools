#! /usr/bin/python
## encoding: utf-8
#
# Copyright (c) 2008 Adeodato Simó (dato@net.com.org.es)
# Licensed under the terms of the MIT license.

"""Generate a Packages stream with all the rdepends for a given transition.

Pass the name of the source package to transition, and the script will figure
out the renamed/disappeared packages by itself. By default the reverse
dependencies will be calculated against unstable and for all arches, but this
can be modified with --suite and --arches.

If a transition is not uploaded to unstable yet, you can:

  (a) pass it as "srcname/experimental" if it's uploaded to experimental.
  (b) pass it as "srcname:oldpk1,oldpkg2[,...] if it's not uploaded at all yet.
"""

import re
import optparse
import subprocess

import apt_pkg
apt_pkg.init()

from lib import dctrl

##

def main():
    options, args = parse_options()
    srcpkg = args[0]

    if srcpkg.endswith('/experimental'):
        dist = 'experimental'
        srcpkg = re.sub(r'/experimental$', '', srcpkg)
    else:
        dist = 'unstable'

    if ':' not in srcpkg:
        disappeared = dctrl.get_disappeared_binaries([srcpkg], dist).keys()
    else:
        srcpkg, pkgs = srcpkg.split(':', 1)
        disappeared = pkgs.split(',')

    ##

    cmd = [ 'grep-archive', '%s:ALL:%s' % (options.suite, options.arches),
            '-FDepends', '-e', '|'.join('(^| )%s($|[ ,])' % (r,) for r in disappeared),
            '-a', '--not', '-S', '-e', '^%s($| )' % (srcpkg,) ]

    p = subprocess.Popen(cmd)
    p.wait()

##

def parse_options():
    p = optparse.OptionParser(usage='%s srcpkg')

    p.add_option('-s', '--suite')
    p.add_option('-a', '--arches',
            help='this should be a suitable expression for grep-archive')

    p.set_defaults(suite='unstable', arches='ALL')

    options, args = p.parse_args()

    if len(args) != 1:
        p.error('need exactly one source package')

    return options, args

##

if __name__ == '__main__':
    main()
